#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# import math
import numpy as np
# import scipy.fftpack as fourier
import matplotlib.pyplot as plt
import function as fc 
from eye_movement import EyeMovement, DataProcessed
# import matplotlib.pyplot as plt
# from sklearn.cluster import KMeans

END = 1

pacientes = ['andres','cecilia','GermanF','GermanP','Mauricio']

nameSignalRaw = '../data/'+str(pacientes[4])+'.mat'
nameSignalPro = '../1/completo.mat'
Fs = 1000


#CONSTRUCCION DE CLASE Y CARGA DE DATOS
#----------------------------------------------
Eyes = EyeMovement() 	#Clase senal cruda
Eyef = DataProcessed()  #Clase senal procesada


plt.show()

fc.readSignals(nameSignalRaw,Eyes,Fs) #Cargo y seteo las senales en Eyes
fc.readStruct(nameSignalPro,Eyef) 	  #Cargo y seteo las senales en Eyef

Eyes.setTimeMove(0.3,0.6,0.5)


#FILTRADO DE SENAL - FILTRO PASA BAJOS
#----------------------------------------------

# 1. Media movil
#---------------------------
L = 15 #Orden del filtro
exs3FMovil = fc.movingAverage(Eyes.exs3,L)
exs6FMovil = fc.movingAverage(Eyes.exs6,L)
syncFMovil = fc.movingAverage(Eyes.sync,L)

if END == 0:
	# Plot senales filtradas y superpuestas
	fc.plotSignalFiltradaOriginal(Eyes,exs3FMovil,exs6FMovil,syncFMovil,'Media Móvil')

# 2. Butterworth
#---------------------------
order = 2
cutoff = 6 # desired cutoff frequency of the filter, Hz

exs3FButte = fc.lowPassButterworth(Eyes.Fs,order,cutoff,Eyes.exs3)
exs6FButte = fc.lowPassButterworth(Eyes.Fs,order,cutoff,Eyes.exs6)
syncFButte = fc.lowPassButterworth(Eyes.Fs,order,cutoff,Eyes.sync)

if END == 0:
	# Plot senales filtradas y superpuestas
	fc.plotSignalFiltradaOriginal(Eyes,exs3FButte,exs6FButte,syncFButte,'Butterworth')

if END == 0:
	# Plot comportamiento en frecuencia
	fc.plotFrequencyResponseButter(Eyes.Fs,order,cutoff,Eyes.time[0],Eyes.exs3,exs3FButte)
	fc.plotFrequencyResponseButter(Eyes.Fs,order,cutoff,Eyes.time[1],Eyes.exs6,exs6FButte)
	fc.plotFrequencyResponseButter(Eyes.Fs,order,cutoff,Eyes.time[2],Eyes.sync,syncFButte)


Eyes.setSignals(exs3FButte,exs6FButte,syncFButte) #sustituyo las senalaes originales por las filtradas  

#DETECCION DE UMBRALES
#----------------------------------------------

#Calculo de propiedades
#----------------------------
Eyes.propEx3s.calculatePropierties(Eyes.exs3)
Eyes.propEx6s.calculatePropierties(Eyes.exs6)
Eyes.propSync.calculatePropierties(Eyes.sync)

if END == 0:
	fc.plotSignalThresholds(Eyes)

#Deteccion de minimos y maximos
#----------------------------

pickMins = fc.detectMin(Eyes)
pickMaxs = fc.detectMax(Eyes)


Eyes.setCoordPicksExs3(pickMaxs[0],pickMins[0])
Eyes.setCoordPicksExs6(pickMaxs[1],pickMins[1])
Eyes.setCoordPicksSync(pickMaxs[2],pickMins[2])



if END == 0:
	fc.plotSignalPicks(Eyes)

#CORTE DE LA SENAL SEGUN LOS UMBRALES Y CLASIFICACION
#-----------------------------------------------
fc.calculateCutSignal(Eyes.exs3,Eyes.propEx3s,Eyes.coordPickExs3,Eyes.Fs,Eyes.signalProccesedExs3)
fc.calculateCutSignal(Eyes.exs6,Eyes.propEx6s,Eyes.coordPickExs6,Eyes.Fs,Eyes.signalProccesedExs6)
fc.calculateCutSignal(Eyes.sync,Eyes.propSync,Eyes.coordPickSync,Eyes.Fs,Eyes.signalProccesedSync)


print(Eyes.signalProccesedExs3.coordCut)
print(Eyes.signalProccesedExs3.senses)
print(Eyes.signalProccesedExs3.data)
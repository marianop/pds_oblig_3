#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import function as fc

class EyeMovement(object):

    def __init__(self):

        self.Fs = None
        self.time = [[],[],[]] #[ex3s,ex6s,sync]

        self.exs3 = []
        self.exs6 = []
        self.sync = []

        self.propEx3s = SignalProperties()
        self.propEx6s = SignalProperties()
        self.propSync = SignalProperties()

        self.coordPickExs3 = [[],[]]
        self.coordPickExs6 = [[],[]]
        self.coordPickSync = [[],[]]

        self.timeMoveExs3 = None
        self.timeMoveExs6 = None
        self.timeMoveSync = None

        self.signalProccesedExs3 = SignalProccesed()
        self.signalProccesedExs6 = SignalProccesed()    
        self.signalProccesedSync = SignalProccesed()


    def setSignals(self,exs_3,exs_6,sync_):
        self.exs3 = exs_3
        self.exs6 = exs_6
        self.sync = sync_

    def setFs(self,fs_):
        self.Fs = fs_

    def setTime(self):
        self.time[0] = np.linspace(0,len(self.exs3)/self.Fs,len(self.exs3))
        self.time[1] = np.linspace(0,len(self.exs6)/self.Fs,len(self.exs6))
        self.time[2] = np.linspace(0,len(self.sync)/self.Fs,len(self.sync))

    def setCoordPicksExs3(self,coordMax,coordMin):
        self.coordPickExs3[0] = coordMin
        self.coordPickExs3[1] = coordMax

    def setCoordPicksExs6(self,coordMax,coordMin):
        self.coordPickExs6[0] = coordMin
        self.coordPickExs6[1] = coordMax
    def setCoordPicksSync(self,coordMax,coordMin):
        self.coordPickSync[0] = coordMin
        self.coordPickSync[1] = coordMax

    def setTimeMove(self,timeExs3,timeExs6,timeSync):
        self.timeMoveExs3 = timeExs3*self.Fs
        self.timeMoveExs6 = timeExs6*self.Fs
        self.timeMoveSync = timeSync*self.Fs        


class SignalProccesed(object):

    def __init__(self):

        self.data = None
        self.senses = None
        self.coordCut = None

    def setSignalProccesed(self,data_,senses_,coord_):
        self.data = data_
        self.senses = senses_
        self.coordCut = coord_

class DataProcessed(object):

    def __init__(self):

        self.data = []
        self.units = None
        self.labels = None
        self.isi = None
        self.isi_units = None
        self.start_sample = None

    def setDataInit(self,dat_,unit_,labe_,isi_,isi_uni_,start_):

        self.data = dat_
        self.units = unit_
        self.labels = labe_
        self.isi = isi_
        self.isi_units = isi_uni_
        self.start_sample = start_

class SignalProperties(object):

    def __init__(self):

        self.K = 1.6  #Numero de std
        self.C = 0.2

        self.mean = None
        self.std  = None
        self.umbr = []
        self.umbr_int = []

    def setProperties(self,mean_,std_,umbr_,umbr_int_):
        self.mean = mean_
        self.std  = std_
        self.umbr = umbr_
        self.umbr_int = umbr_int_


    def calculatePropierties(self,signal):

        mean_s = np.mean(signal)
        std_s  = np.std(signal)
        umbral_s = [mean_s - self.K*std_s , mean_s+self.K*std_s]
        umbr_int_ = [mean_s - self.C*std_s, mean_s+self.C*std_s]

        self.setProperties(mean_s,std_s,umbral_s,umbr_int_)
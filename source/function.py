#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import string
import scipy.signal as sg
import csv
import matplotlib.pyplot as plt
from eye_movement import EyeMovement
import ast
from sklearn.cluster import KMeans
import scipy.io
from scipy.signal import butter, lfilter, freqz,find_peaks_cwt

import sys
sys.path.insert(1, r'./../functions')  # add to pythonpath
from detect_peaks import detect_peaks



def readSignals(name,eyeMov,fs):
    mat   = scipy.io.loadmat(name)
    exs3 = mat['Ex3s'][0]
    exs6 = mat['Ex6s'][0]
    sync = mat['Sync'][0]

    exs3 = (exs3 - np.mean(exs3))/max(exs3)    
    exs6 = (exs6 - np.mean(exs6))/max(exs6)
    sync = (sync - np.mean(sync))/max(sync)

    eyeMov.setSignals(exs3,exs6,sync)
    eyeMov.setFs(fs)
    eyeMov.setTime()

def readStruct(name,DataCompr):
    mat   = scipy.io.loadmat(name)

    data = mat['data']
    units = mat['units'][0]
    labels = mat['labels'][0]
    isi = mat['isi'][0][0]
    isi_units = mat['isi_units'][0]
    start_sample = mat['start_sample'][0][0]

    DataCompr.setDataInit(data,units,labels,isi,isi_units,start_sample)
    



def printDataSignalProccesed(Eyef):

    print('--------------------------------------')
    print('| DATA SIGNAL PROCESSED              |')
    print('|-------------------------------------')
    print('| Unitis      |  '+str(Eyef.units)+  '                  |')
    print('| Labels      | ',Eyef.labels,' |')
    print('| Isi         | ',Eyef.isi,'                  |')
    print('| Isi Unitis  | ',Eyef.isi_units,'                 |')
    print('| Start samp  | ',Eyef.start_sample,'             |')
    print('--------------------------------------')



def butter_lowpass(cutoff, fs, order):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order):#t=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y

def movingAverage(y,L):
    inM = math.ceil(L/2)
    fiM = math.floor(L/2)
    ys = np.zeros(len(y))

    for i in range(inM,len(y)-inM):
        ys[i] = np.mean(y[i-fiM:i+fiM])
    return ys

def plotSignalFiltradaOriginal(Eyes,exs3F,exs6F,syncF,nameFilter):

    plt.figure()
    plt.plot(Eyes.time[0],Eyes.exs3,label = 'X[n]')
    plt.plot(Eyes.time[0],exs3F,label= 'Y[n]')
    plt.title('Exs3 (original) vs Exs3 ('+str(nameFilter)+')')
    plt.xlabel('Tiempo (s)')
    plt.legend()

    plt.figure()
    plt.plot(Eyes.time[1],Eyes.exs6,label = 'X[n]')
    plt.plot(Eyes.time[1],exs6F,label= 'Y[n]')
    plt.title('Exs6 (original) vs Exs6 ('+str(nameFilter)+')')
    plt.xlabel('Tiempo (s)')
    plt.legend()

    plt.figure()
    plt.plot(Eyes.time[2],Eyes.sync,label = 'X[n]')
    plt.plot(Eyes.time[2],syncF,label= 'Y[n]')
    plt.title('Sync (original) vs Sync ('+str(nameFilter)+')')
    plt.xlabel('Tiempo (s)')
    plt.legend()
    plt.show()


def lowPassButterworth(Fs,order,cutoff,signal):

    b, a = butter_lowpass(cutoff, Fs, order)
    w, h = freqz(b, a, worN=8000)
    y    = butter_lowpass_filter(signal, cutoff, Fs, order)
    return y

def plotFrequencyResponseButter(Fs, order, cutoff, time, signal, y):

    b, a = butter_lowpass(cutoff, Fs, order)
    w, h = freqz(b, a, worN=8000)

    plt.subplot(2, 1, 1)
    plt.plot(0.5*Fs*w/np.pi, np.abs(h), 'b')
    plt.plot(cutoff, 0.5*np.sqrt(2), 'ko')
    plt.axvline(cutoff, color='k')
    plt.xlim(0, 0.5*Fs)
    plt.title("Lowpass Filter Frequency Response")
    plt.xlabel('Frequency [Hz]')
    plt.grid()

    plt.subplot(2, 1, 2)
    plt.plot(time, signal, 'b-', label='data')
    plt.plot(time, y, 'g-', linewidth=2, label='filtered data')
    plt.xlabel('Time [sec]')
    plt.grid()
    plt.legend()

    plt.show()

def plotSignalThresholds(Eyes):

    plt.figure()
    plt.plot(Eyes.time[0],Eyes.exs3,label = 'X[n]')
    plt.axhline(y=Eyes.propEx3s.umbr[0], color='r', linestyle='-')
    plt.axhline(y=Eyes.propEx3s.umbr[1], color='r', linestyle='-')
    plt.axhline(y=Eyes.propEx3s.mean, color='g', linestyle='-')
    plt.axhline(y=Eyes.propEx3s.umbr_int[0], color='y', linestyle='-')
    plt.axhline(y=Eyes.propEx3s.umbr_int[1], color='y', linestyle='-')
    plt.title('Signal Exs3 + thresholds')
    plt.xlabel('Tiempo (s)')
    plt.legend()

    plt.figure()
    plt.plot(Eyes.time[1],Eyes.exs6,label = 'X[n]')
    plt.axhline(y=Eyes.propEx6s.umbr[0], color='r', linestyle='-')
    plt.axhline(y=Eyes.propEx6s.umbr[1], color='r', linestyle='-')
    plt.axhline(y=Eyes.propEx6s.umbr_int[0], color='y', linestyle='-')
    plt.axhline(y=Eyes.propEx6s.umbr_int[1], color='y', linestyle='-')
    plt.axhline(y=Eyes.propEx6s.mean, color='g', linestyle='-')
    plt.title('Signal Exs6 + thresholds')
    plt.xlabel('Tiempo (s)')
    plt.legend()

    plt.figure()
    plt.plot(Eyes.time[2],Eyes.sync,label = 'X[n]')
    plt.axhline(y=Eyes.propSync.umbr[0], color='r', linestyle='-')
    plt.axhline(y=Eyes.propSync.umbr[1], color='r', linestyle='-')
    plt.axhline(y=Eyes.propSync.umbr_int[0], color='y', linestyle='-')
    plt.axhline(y=Eyes.propSync.umbr_int[1], color='y', linestyle='-')
    plt.axhline(y=Eyes.propSync.mean, color='g', linestyle='-')
    plt.title('Signal Sync + thresholds')
    plt.xlabel('Tiempo (s)')
    plt.legend()
    plt.show()



def plotSignalPicks(Eyes):
    plt.figure()
    plt.plot(Eyes.time[0],Eyes.exs3)
    plt.axhline(y=Eyes.propEx3s.umbr[0], color='r', linestyle='-')
    plt.axhline(y=Eyes.propEx3s.umbr[1], color='r', linestyle='-')
    plt.axhline(y=Eyes.propEx3s.mean, color='g', linestyle='-')
    plt.axhline(y=Eyes.propEx3s.umbr_int[0], color='y', linestyle='-')
    plt.axhline(y=Eyes.propEx3s.umbr_int[1], color='y', linestyle='-')
    for i in range(len(Eyes.coordPickExs3[0])):
        plt.plot(Eyes.time[0][Eyes.coordPickExs3[0][i]],Eyes.exs3[Eyes.coordPickExs3[0][i]],'ro')
        plt.axvline(Eyes.time[0][Eyes.coordPickExs3[0][i]],linestyle='-.',color = 'm')
    for i in range(len(Eyes.coordPickExs3[1])):
        plt.plot(Eyes.time[0][Eyes.coordPickExs3[1][i]],Eyes.exs3[Eyes.coordPickExs3[1][i]],'go')
        plt.axvline(Eyes.time[0][Eyes.coordPickExs3[1][i]],linestyle='-.',color = '#b30000')
   
    plt.title('Signal Exs3 + thresholds')
    plt.xlabel('Tiempo (s)')

    plt.figure()
    plt.plot(Eyes.time[1],Eyes.exs6)
    plt.axhline(y=Eyes.propEx6s.umbr[0], color='r', linestyle='-')
    plt.axhline(y=Eyes.propEx6s.umbr[1], color='r', linestyle='-')
    plt.axhline(y=Eyes.propEx6s.mean, color='g', linestyle='-')
    plt.axhline(y=Eyes.propEx6s.umbr_int[0], color='y', linestyle='-')
    plt.axhline(y=Eyes.propEx6s.umbr_int[1], color='y', linestyle='-')
    for i in range(len(Eyes.coordPickExs6[0])):
        plt.plot(Eyes.time[1][Eyes.coordPickExs6[0][i]],Eyes.exs6[Eyes.coordPickExs6[0][i]],'ro')
        plt.axvline(Eyes.time[1][Eyes.coordPickExs6[0][i]],linestyle='-.',color = 'm')
    for i in range(len(Eyes.coordPickExs6[1])):
        plt.plot(Eyes.time[1][Eyes.coordPickExs6[1][i]],Eyes.exs6[Eyes.coordPickExs6[1][i]],'go')
        plt.axvline(Eyes.time[1][Eyes.coordPickExs6[1][i]],linestyle='-.',color = '#b30000')

    plt.title('Signal Exs6 + thresholds')
    plt.xlabel('Tiempo (s)')

    plt.figure()
    plt.plot(Eyes.time[2],Eyes.sync)
    plt.axhline(y=Eyes.propSync.umbr[0], color='r', linestyle='-')
    plt.axhline(y=Eyes.propSync.umbr[1], color='r', linestyle='-')
    plt.axhline(y=Eyes.propSync.mean, color='g', linestyle='-')
    plt.axhline(y=Eyes.propSync.umbr_int[0], color='y', linestyle='-')
    plt.axhline(y=Eyes.propSync.umbr_int[1], color='y', linestyle='-')

    for i in range(len(Eyes.coordPickSync[0])):
        plt.plot(Eyes.time[2][Eyes.coordPickSync[0][i]],Eyes.sync[Eyes.coordPickSync[0][i]],'ro')
        plt.axvline(Eyes.time[2][Eyes.coordPickSync[0][i]],linestyle='-.',color = 'm')    
    for i in range(len(Eyes.coordPickSync[1])):
        plt.plot(Eyes.time[2][Eyes.coordPickSync[1][i]],Eyes.sync[Eyes.coordPickSync[1][i]],'go')
        plt.axvline(Eyes.time[2][Eyes.coordPickSync[1][i]],linestyle='-.',color = '#b30000')
   
    plt.title('Signal Sync + thresholds')
    plt.xlabel('Tiempo (s)')
    plt.show()

def detectMin(Eyes):

    minExs3 = calculateMin(Eyes.exs3,Eyes.propEx3s.umbr[0])#  mpd = int(Rmean*0.75), valley=True)
    minExs6 = calculateMin(Eyes.exs6,Eyes.propEx6s.umbr[0])
    minSync = calculateMin(Eyes.sync,Eyes.propSync.umbr[0])

    return [minExs3,minExs6,minSync]

def detectMax(Eyes):

    minExs3 = calculateMax(Eyes.exs3,Eyes.propEx3s.umbr[1])#  mpd = int(Rmean*0.75), valley=True)
    minExs6 = calculateMax(Eyes.exs6,Eyes.propEx6s.umbr[1])
    minSync = calculateMax(Eyes.sync,Eyes.propSync.umbr[1])

    return [minExs3,minExs6,minSync]

def calculateMax(sign, thre):
    maxValue = []
    for i in range(1,len(sign)-1):
        if (sign[i] > sign[i-1]) and (sign[i] > sign[i+1]) and (sign[i] > thre):
            maxValue.append(i)        
    return maxValue


def calculateMin(sign, thre):
    minValue = []
    for i in range(1,len(sign)-1):
        if (sign[i] < sign[i-1]) and (sign[i] < sign[i+1]) and (sign[i] < thre):
            minValue.append(i)
    return minValue


def calculateCutSignal(x,proP,coordPicks,Fs,signalProccesed):

    ladH = [0,1] #Principio = 0 / Final = 1
    ladV = [0,1] #Principio Abajo = 0 / Principio Arriba = 1
    tipM = ['Izq-Der','Der-Izq']

    coordCut = [] #Coordenadas de corte
    modeSens = [] #Modo: de izq a dere o de dere a izq
    signaCut = [] #Todos los eventos de la senal ya cortados

    #1. Busco todos los cortes de la senal y corto
    #----------------------------------------------------------
    for i in range(len(coordPicks[0])):
        ind = i
        if (coordPicks[1][ind] < coordPicks[0][ind]):
            indIn = buscoCorte(x,coordPicks[1][ind],proP,ladH[0],ladV[1],Fs)
            indFn = buscoCorte(x,coordPicks[0][ind],proP,ladH[1],ladV[1],Fs)
            signaCut.append(x[indIn:indFn])
            coordCut.append([indIn,indFn])
            modeSens.append(tipM[0])
        else:
            indFn = buscoCorte(x,coordPicks[1][ind],proP,ladH[1],ladV[0],Fs)
            indIn = buscoCorte(x,coordPicks[0][ind],proP,ladH[0],ladV[0],Fs)
            coordCut.append([indIn,indFn])
            signaCut.append(x[indIn:indFn])
            modeSens.append(tipM[1])

    #2. Guardo los eventos, las coord de los eventos y los modos
    #--------------------------------------------------------------
    signalProccesed.setSignalProccesed(signaCut,modeSens,coordCut)



def buscoCorte(x,ind,porP,ladH,ladV,Fs):

    TEMPO = int(0.1*Fs)

    if ladV == 1:
        if ladH == 0:    
            for i in range(ind,0,-1):
                if (x[i] <= porP.mean) or (x[i] <= porP.umbr_int[1]):
                    return int(i-TEMPO)
        if ladH == 1:
            for i in range(ind,len(x)):
                if (x[i] >= porP.mean) or (x[i] >= porP.umbr_int[0]):
                    return int(i+TEMPO)

    if ladV == 0:
        if ladH == 0:
            for i in range(ind,0,-1):
                if (x[i] >= porP.mean) or (x[i] >= porP.umbr_int[0]):
                    return int(i-TEMPO)
        if ladH == 1:
            for i in range(ind,len(x)):
                if (x[i] <= porP.mean) or (x[i] <= porP.umbr_int[1]):
                    return int(i+TEMPO)
